#!/usr/bin/env python
# -*- coding: utf-8 -*-
# crawl everything from wenku
import multiprocessing, requests, time, os, subprocess, re, json, shutil, string
from bs4 import BeautifulSoup

outDir = 'wenku'
url = 'http://bbs.jjwxc.net/showmsg.php?board=7&id=%d&page=%d'
idxUrl = 'http://bbs.jjwxc.net/board.php?board=7&page=%d'

try:
	proxies = json.load(open('proxies.json'))
except:
	proxies = []

try:
	updateMap = json.load('updateMap.json')
except:
	updateMap = {}

class fetchThread(multiprocessing.Process):
	def __init__(self, threadID, tid, proxy):
		self.threadID = threadID
		self.tid = tid
		self.proxy = proxy
		multiprocessing.Process.__init__(self)

	def dumpSoup(self, soup, fout):
		try:
			fout.write(soup.title.get_text().encode('utf8') + '\n')
		except:
			fout.close()
			print('no title')
			return None
		# fout.write('%sshowmsg.php?board=7&id=%s&page=%d\n' % (base, tid, pid))
		for read in soup.select('td.read'):
			text = read.encode('utf8')
			text = re.sub('\s{5,}', '', text)
			text = re.sub('(<br/>|<div id="topic">|</div>)', '\n', text)
			text = re.sub('</td>', '', text)
			text = re.sub('<td class="read">', '', text)
			text = re.sub('(\n\s*){2,}', '\n', text)
			fout.write(text + '\n')
			date = read.find_parent("table").find_parent("tr").find_next_sibling('tr').get_text().encode('utf8') 
			date = re.sub('(\n\s*){2,}', '\n', date)
			fout.write(date + '-'*19 + '=' + '-' * 20 + '\n')
		fout.close()


	def readP(self, tid, pid, s):
		try:
			resp = s.get(url % (tid, pid), timeout = 30, \
				proxies = self.proxy)
		except Exception, e:
			print('request failed for %d at page %d' % (tid, pid))
			# return None
			raise e

		resp.encoding = 'gb18030'
		soup = BeautifulSoup(resp.text)

		try:
			soup.title.get_text().encode('utf8')
			fout =  pen('%s/%d-%d.txt' % (outDir, tid, pid), 'w')
		except:
			print('no title')
			return None
		if (not self.dumpSoup(soup, fout)):
			return None

	# read the first page of a given thread, if it is the only page, stop
	# otherwise, continue to read the following pages
	# e.g. 59523
	def readT(self, tid, s):
		try:
			resp = s.get(url % (tid, 0),\
		    		timeout=30, proxies = self.proxy)
		except Exception, e:
			print('request failed for %d' % tid)
			# return None
			raise e

		resp.encoding = 'gb18030'
		soup = BeautifulSoup(resp.text)
		try:
			lastPage = soup.find(id = 'pager_top').find_all('a')[-1].get('href')
			lastPage = int(re.search(r'page=(\d+)', lastPage).group(1))
		except:
			lastPage = 0
		
		try:
			soup.title.get_text().encode('utf8')
			fout = open('%s/%d-%d.txt' % (outDir, tid, 0), 'w')
		except:
			print('no title')
			return None
		if (not self.dumpSoup(soup, fout)):
			return None

		for pid in range(1, lastPage + 1):
			time.sleep(10)
			self.readP(tid, pid, s)

	def run(self):
		s = requests.Session()
		self.readT(self.tid, s)

def testProxy(proxy):
	try:
		resp = s.get('http://www.baidu.com', timeout = 30, \
			proxies = proxy)
	except Exception, e:
		return False
	return True
# for any given thread, record the latest update time
# updateMap[tid] = time.strftime('%Y-%m-%d %H:%M:%S')

# thread = fetchThread(1, 59528, {'https' : 'https://181.88.177.145:8080'})
# thread.start()
# thread.join()

def fetchIndex(s, pid, proxy):
	# try:
	# 	resp = s.get(idxUrl % pid,\
	#     		timeout=30, proxies = proxy)
	# except Exception, e:
	# 	print('request failed for page %d' % pid)
	# 	# return None
	# 	raise e

	# resp.encoding = 'gb18030'
	# open('tmp.txt', 'w').write(resp.text.encode('utf8'))
	# soup = BeautifulSoup(resp.text)
	soup = BeautifulSoup(open('tmp.txt').read().decode('utf8'))
	count = 0
	for table in soup.select('table[width=800]'):
		print(table)

s = requests.Session()
fetchIndex(s, 1, {'https' : 'https://181.88.177.145:8080'})
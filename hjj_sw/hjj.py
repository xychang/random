#!/usr/bin/env python
# -*- coding: utf-8 -*-
# crawl useful things about hjj
import requests, time, os, subprocess, re, smtplib, json, shutil, string
from bs4 import BeautifulSoup
from email.mime.text import MIMEText

# urls = ['http://bbs.jjwxc.net/showmsg.php?board=3&id=754537&page=0', 'http://bbs.jjwxc.net/showmsg.php?board=3&id=754537&page=1', 'http://bbs.jjwxc.net/showmsg.php?board=3&id=754537&page=2', 'http://bbs.jjwxc.net/showmsg.php?board=3&id=754537&page=3']
urls = ['http://bbs.jjwxc.net/search.php?act=search&board=3&keyword=%C9%A8%CE%C4%D0%A1%D4%BA&topic=2&submit=%B2%E9%D1%AF', 'http://bbs.jjwxc.net/search.php?act=search&board=3&keyword=%C9%A8%CE%C4%D0%A1%D4%BA&topic=1&submit=%B2%E9%D1%AF', 'http://bbs.jjwxc.net/search.php?act=search&board=3&keyword=%C9%A8%CE%C4%D0%A1%D4%BA&topic=3&submit=%B2%E9%D1%AF']
base = 'http://bbs.jjwxc.net/'

lastSent = '1970-01-01 00:00:00'
try:
	toSend = json.load(open('toSend.json'))
except:
	toSend = set([])

try:
	fileDateM = json.load(open('fileDate.json'))
except:
	fileDateM = {}

# http://bbs.jjwxc.net/showmsg.php?board=3&id=759756&page=1
def readT(tid, pid, level, fout):
	try:
		if pid > 0:
			resp = s.get(base + 'showmsg.php?board=3&id=%s&page=%d' % (tid, pid),
	    		timeout=20)
		else:
			resp = s.get(base + 'showmsg.php?board=3&id=%s' % (tid),
	    		timeout=20)
	except:
		print('request failed')
		return None
	resp.encoding = 'gb18030'
	soup = BeautifulSoup(resp.text)
	newUrls = [x.get('href') for x in soup.find_all('a') if x not in doneSet]
	if level < 1:
		for tUrl in newUrls:
			todoMap[tUrl] = level + 1
	try:
		fout.write(soup.title.get_text().encode('utf8') + '\n')
	except:
		fout.close()
		print('no title')
		return None
	fout.write('%sshowmsg.php?board=3&id=%s&page=%d\n' % (base, tid, pid))
	for read in soup.select('td.read'):
		text = read.encode('utf8')
		text = re.sub('\s{5,}', '', text)
		text = re.sub('(<br/>|<div id="topic">|</div>)', '\n', text)
		text = re.sub('</td>', '', text)
		text = re.sub('<td class="read">', '', text)
		text = re.sub('(\n\s*){2,}', '\n', text)
		fout.write(text + '\n')
		date = read.find_parent("table").find_parent("tr").find_next_sibling('tr').get_text().encode('utf8') 
		date = re.sub('(\n\s*){2,}', '\n', date)
		fout.write(date + '-'*40 + '\n')
	fout.close()

	# return the latest post date
	try:
		date = re.search(r'(\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2})\D+$', date).group(1)
	except:
		fout.close()
		print('no date')
		return None
	return date

# showmsg.php?board=3&keyword=扫文小院&id=758143#2015-02-24 13:40:44
# return a new pid, start from pid, stop until the tread's date is later or equal to the interestD
def readST(tid, pid, level, interestD, fout):
	existPs = []
	for i in range(10):
		if (os.path.isfile('%s-%d.txt' % (tid, i))):
			existPs.append(i)
		else:
			break
	pid = max(existPs) if len(existPs) > 0 else 0
	while (pid <= 10):
		date = readT(tid, pid, level, open(fout, 'w'))
		if (not date):
			return -1
		if (parseDate(date) >= parseDate(interestD)):
			return pid
		checkAndStore(tid, pid, fout)
		pid += 1
	return -1

def getDate(x):
	try:
		return x.parent.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling
	except:
		return None

def parseDate(dateS):
	return time.strptime(dateS, '%Y-%m-%d %H:%M:%S')

def extractAround(keyword, text):
	if (len(text) < 1000):
		return text
	# print(text)
	lineNo = r'№(\d+) ☆☆☆'

	blocks = text.split('\n'+'-'*40+'\n')

	blockIds = [(idx, re.findall(lineNo, blocks[idx])) for idx in range(len(blocks))]
	hitIdx = [idx for idx in range(len(blocks)) if blocks[idx].find(keyword) >= 0]
	if len(hitIdx) == 0:
		return '疑似误抓\n' + blocks[0]
	if 0 in hitIdx:
		return text
	context = 3
	# the blocks to be displayed
	disIdx = sorted(list(set([0]) | reduce(lambda x,y: x | y, [set(range(idx - context, idx + context + 1)) for idx in hitIdx]) & set(range(len(blocks)))))
	# print(blocks[97])

	return('\n'+'-'*40+'\n').join([blocks[idx] for idx in disIdx])

# print(extractAround('扫文小院', open('759938-2.txt').read()))

def sendEmail(fin):
	global lastSent
	keyword = '扫文小院'
	# keyword = '咪子鱼'
	try:
		toSend = set(json.load(open('toSend.json')))
	except:
		toSend = set([])
	if (fin):
		toSend.add(fin)
	elif(len(toSend) > 0):
		oneDay = 3600 * 24
		if (time.mktime(parseDate(lastSent)) + oneDay < time.time()):
			lastSent = time.strftime('%Y-%m-%d %H:%M:%S')

			data = ''
			for thfile in toSend:
				data += '%s\n\n' % extractAround(keyword,open(thfile).read())
			# title = data.split('\n')[0]
			data = re.sub('\n', '\n<br>\n', data)
			data = re.sub(keyword, '<b><font color="red">%s</font></b>' % keyword, data)

			msg = MIMEText(data, 'html', 'utf-8')
			# msg = MIMEText(data) 
			msg['Subject'] = '%s#hjj' % time.strftime('%Y-%m-%d')
			msg['from'] = 'xiaoyun.anye@gmail.com'
			msg['To'] = 'xiaoyun.anye@gmail.com, frostmirror@gmail.com'

			s = smtplib.SMTP('smtp.gmail.com:587')
			s.ehlo()
			s.starttls()
			s.login('xiaoyun.anye@gmail.com', open('../apppass.txt').read().strip())
			s.sendmail('xiaoyun.anye@gmail.com', ['xiaoyun.anye@gmail.com','frostmirror@gmail.com'], msg.as_string())
			s.quit()
			toSend = set([])

	json.dump(list(toSend), open('toSend.json', 'w'))

def mergeFile(fileA, fileB, fileM):
	lineNo = r'№(\d+) ☆☆☆'
	linePat = '№%s ☆☆☆'
	noA = re.findall(lineNo + r'[^\n]+\n\-{40}\n', open(fileA).read())
	# print(noA)
	noB = re.findall(lineNo + r'[^\n]+\n\-{40}\n', open(fileB).read())
	# print(noB)
	if len(set(noA) - set(noB)) == 0:
		if (not fileB == fileM):
			shutil.copy2(fileB, fileM)
		return [idxB for idxB in noB if idxB in (set(noB) - set(noA))]
	if len(set(noB) - set(noA)) == 0:
		if (not fileA == fileM):
			shutil.copy2(fileA, fileM)
		return [idxA for idxA in noA if idxA in (set(noA) - set(noB))]
	# other wise, we need serious merge
	print(len(set(noA) - set(noB)), len(set(noB) - set(noA)))
	print(([idxB for idxB in noB if idxB in (set(noB) - set(noA))], [idxA for idxA in noA if idxA in (set(noA) - set(noB))]))
	# use fileA as basis
	# search for deleted entries
	text = open(fileA).read()
	for idx in set(noA) - set(noB):
		text = re.sub('(?<!已删除\n)'+linePat % idx, '已删除\n' + linePat % idx, text)
	# get all added entries
	textB = open(fileB).read()
	firstB = [idxB for idxB in noB if idxB in (set(noB) - set(noA))][0]
	fout = open(fileM, 'w')
	fout.write(text)
	fout.write('脚本更新：')
	fout.write(textB[string.rfind(textB, '\n' + '-'*40 + '\n', 0, string.find(textB, linePat % firstB)) + 41:])
	fout.close()
	return (len(set(noA) - set(noB)))

	# for seg in segA:
	# 	try:
	# 		idx = re.match()


	# textA = open(fileA).readlines()
	# textB = open(fileB).readlines()
	# print([re.findall(lineNo, textAl) for textAl in textA])

# print(mergeFile('759356-1.txt', '759356-0.txt', '759356-m3.txt'))

def checkAndStore(tid, pid, inFile, send = True):
	fileS = 0
	if (os.path.isfile('%s-%s.txt' % (tid, pid))):
		fileS = os.stat('%s-%s.txt' % (tid, pid)).st_size
	if (fileS > 0):
		print(mergeFile('%s-%s.txt' % (tid, pid), inFile, '%s-%s.txt' % (tid, pid)))
	else:
		subprocess.call(['mv', inFile, '%s-%s.txt' % (tid, pid)])
	if (send):
		sendEmail('%s-%s.txt' % (tid, pid))
	time.sleep(60)

def crawl():
	global s, doneSet
	for url in urls:
		s = requests.Session()
		try:
			resp = s.get(url, timeout = 20)
		except:
			print('request failed for %s' % url)
			continue
		resp.encoding = 'gb18030'
		# http://bbs.jjwxc.net/showmsg.php?board=3&id=740007 
		# find the text urls as well
		# matches = re.findall(r'(jjwxc\.net/showmsg\.php[^\n<>\s]+id=\d+)',resp.text)
		# print(matches)

		soup = BeautifulSoup(resp.text)
		doneSet = set([])
		toVisits = ([(x.get('href'), getDate(x)) for x in soup.find_all('a')])
		todoMap = {}
		dateUrlMap = {}
		for (tUrl, date) in toVisits:
			todoMap[tUrl] = 1
			if (date):
				dateUrlMap[tUrl] = date.text.strip()
				# json.dump(fileDateM, open('fileDate.json', 'w'))

		# print(dateUrlMap)
			
		while len(todoMap) > 0:
			href = todoMap.keys()[0]
			level = todoMap[href]
			del todoMap[href]
			doneSet.add(href)
			# href = link.get('href')
			pid = 0
			if (not href):
				continue
			# print(href.encode('utf8'))
			try:
				match = re.search(r'showmsg\.php.*&id=(\d+)', href)
			except:
				match = None
			if not match:
				try:
					match = re.search(r'\?board=\d+.*&id=(\d+)', href)
				except:
					match = None
			try:
				matchP = re.search(r'&page=(\d+)', href)
				if (matchP):
					pid = matchP.group(1)
			except:
				pass

			# check if there is #date in url
			interestD = None
			try:
				matchD = re.search(r'#(\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2})', href)
				if matchD:
					interestD = matchD.group(1)
			except:
				pass

			if(match and href):
				tid = (match.group(1).encode('utf8'))
				# print(tid, pid, level)
				fileS = 0
				if (os.path.isfile('%s-%s.txt' % (tid, pid))):
					fileS = os.stat('%s-%s.txt' % (tid, pid)).st_size

				# if the file is empty or the file is old
				# try:
				# 	print(fileDateM['%s-%s' % (tid, pid)], dateUrlMap[href])
				# except:
				# 	print('new file')

				# check id we already have this file, if not, crawl it
				if (fileS == 0 or (href in dateUrlMap and '%s-%s' % (tid, pid) in fileDateM\
					and parseDate(fileDateM['%s-%s' % (tid, pid)]) < parseDate(dateUrlMap[href]))):
					# check when there is a date to the comment, if the date is later than the recorded date
					if (interestD):
						send = (not '%s-%s' % (tid, pid) in fileDateM) or parseDate(interestD) > parseDate(fileDateM['%s-%s' % (tid, pid)])
					else:
						send = True

					if (send):
						print('getting new data on %s-%s' % (tid, pid))

					if (href in dateUrlMap):
						fileDateM['%s-%s' % (tid, pid)] = dateUrlMap[href]
						json.dump(fileDateM, open('fileDate.json', 'w'))

					if (not interestD):
						# we want the whole thread
						if readT(tid, pid, level, open('tmp.txt', 'w')):
							checkAndStore(tid, pid, 'tmp.txt')
					else:
						# we want to find the post of interest
						newPid = readST(tid, pid, level, interestD, 'tmp.txt')
						if (newPid < 0):
							continue
						checkAndStore(tid, newPid, 'tmp.txt', send)
					# if (fileS == 0 and os.stat('tmp.txt').st_size > 0):
					# 	sendEmail(open('tmp.txt'))


		time.sleep(3)
	sendEmail(None)
	print('finished crawling %s' % time.strftime('%Y-%m-%d %H:%M:%S'))

while True:
	crawl()
	time.sleep(3600)
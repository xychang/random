# -*- coding: utf-8 -*-
import jieba, datetime, collections, json, cPickle, math, re

printList = lambda x: u','.join(x).encode('utf-8')

def addToDict(words, d):
	for word in words:
		if len(word) <= 1:
			continue
		d[word] += 1

def addToDictChi(words, d):
	for word in words:
		d[word] += 1


def addToDictMult(words, mult, d):
	for word in words:
		if len(word) <= 1:
			continue
		d[word] += mult

def substrings(word):
	l = []
	for idx in range(min(2, len(word) / 2), len(word) + 1):
		l.extend([word[st:st+idx] for st in range(len(word) - idx + 1)])
	return l

# print(printList(substrings(u'画风绯闻')))

def getWords():
	wordDict = collections.defaultdict(int)
	totalCounter = cPickle.load(open('totalCounter.pkl'))
	total = 0

	for line in open('../../data/XQ帖子列表.txt'):
		if line[0] == '#':
			continue
		line = line.strip().split('\t')
		if (len(line) != 8):
			continue
		title = line[1].decode('utf-8').lower()
		
		total += 1
		addToDictChi(title, wordDict)
		addToDictChi([title[idx:idx+2] for idx in range(len(title) - 1)], wordDict)
		addToDictChi([title[idx:idx+3] for idx in range(len(title) - 2)], wordDict)
		addToDictChi([title[idx:idx+4] for idx in range(len(title) - 3)], wordDict)
		addToDictChi([title[idx:idx+5] for idx in range(len(title) - 4)], wordDict)
		addToDictChi([title[idx:idx+6] for idx in range(len(title) - 5)], wordDict)
	wordProb = {}
	# print(wordDict)

	for word in wordDict:
		if (len(word) >= 2):
			
			counts = max([totalCounter[x] for x in substrings(word)])
			if (counts == 0):
				counts = 1
			prob = float(wordDict[word]) / ((float(counts)/total) ** len(word))
		
			if (wordDict[word] > 5) and ((float(wordDict[word])+1)/(counts+1)) > 5:
				wordProb[word] = prob

	for item in (sorted(wordProb.items(), key = lambda x: x[1], reverse = True)[:50]):
		print('%s\t%f' % item)

getWords()

def countFile():

	wordDict = {}
	weightDict = {}
	totalCounter = collections.defaultdict(int)

	jieba.load_userdict('../../data/动漫.txt')
	jieba.load_userdict('../../data/网络新词.txt')
	for line in [u'^-^', u'^_^',u'快男',u'士兵突击',u'j家',u'布拉特',u'吉拉迪诺']:
		jieba.add_word(line.strip())
	errorList = []
	lineNo = 0
	allLines = []
	for line in open('../../data/XQ帖子列表.txt'):
		orgLine = line
		lineNo += 1
		if line[0] == '#':
			continue
		line = line.strip().split('\t')
		if (len(line) != 8):
			# print(orgLine)
			errorList.append(lineNo)
			continue
		title = line[1].lower()
		date = line[3]
		try:
			date = datetime.datetime.strptime(date, '%Y/%m/%d %H:%M')
		except Exception, e:
			errorList.append(lineNo)
			# print(orgLine)
			continue
		allLines.append(line)
		# print(title)
		seg_list = list(jieba.cut_for_search(title))
		# print(printList(seg_list))
		addToDict(seg_list, totalCounter)
		if not (date.year, date.month) in wordDict:
			wordDict[(date.year, date.month)] = collections.defaultdict(int)
			weightDict[(date.year, date.month)] = collections.defaultdict(int)
		# print(printList(seg_list))
		addToDict(seg_list, wordDict[(date.year, date.month)])
		addToDictMult(seg_list, int(line[4]),weightDict[(date.year, date.month)])
		# print(wordDict[(date.year, date.month)])
		# break

	# print(errorList)
	cPickle.dump(wordDict, open('wordDict.pkl','w'))
	cPickle.dump(totalCounter, open('totalCounter.pkl', 'w'))
	cPickle.dump(weightDict, open('weightDict.pkl', 'w'))

def getStream():
	countFile()
	# wordDict = cPickle.load(open('wordDict.pkl'))
	wordDict = cPickle.load(open('weightDict.pkl'))
	totalCounter = cPickle.load(open('totalCounter.pkl'))

	count = 0
	stopwords = set(map(lambda x: x.strip().decode('utf-8'), open('stopwords.txt').readlines()))
	stopwords.add(' ')
	# for item in sorted(totalCounter.items(), key = lambda x: x[1], reverse = True):
	# 	if not item[0] in stopwords:
	# 		print(item[0])
	# 	# print(item[1])
	# 	count += 1
	# 	if (count > 200):
	# 		break

	sumScore = collections.defaultdict(float)
	for key in sorted(wordDict.keys()):
		words = sorted(wordDict[key].items(), key = lambda x: float(x[1]) / math.log(1+totalCounter[x[0]]) ** 2,\
		 reverse = True)
		words = [x for x in words if not x[0] in stopwords and not re.match(r'\d+$', x[0])]
		# print(key)
		words = words[:20]
		for (word, score) in words:
			sumScore[word] += score
	interests = []
	for (word, score) in sorted(sumScore.items(), key = lambda x: x[1], reverse = True):
		# print(score)
		if score > 10000:
			interests.append(word)
	# interests = [u'奥运',u'广播剧',u'快男',u'赛期',u'地震',u'理赔',u'中文',u'声饭',u'shcc',u'mamo',u'魔术师',u'瞪瞪',u'体操',u'做主',u'耽美话',u'魔天楼',u'诈死',u'月关',u'观真',u'无休止',u'梦寒',u'桑托斯',u'家来',u'十七届',u'第二十三届',u'仙五后',u'腐宅',u'stronger',u'messi',u'托斯',u'多斯',u'文并',u'懒猫',u'家求',u'宝前',u'基绊',u'常欣']
	print(printList(interests))
	# fout = open('strea5.csv', 'w')
	# fout.write('key,value,date\n')
	# for key in sorted(wordDict.keys()):
	# 	if (not key[1] % 4 == 1):
	# 		continue
	# 	for word in interests:
	# 		fout.write('%s,%f,%s\n' % (word.encode('utf8'), \
	# 			sum([wordDict[(key[0], key[1]+inc)][word] for inc in range(4) \
	# 				if (key[0], key[1]+inc) in wordDict]), '%02d/01/%02d' % (key[1], key[0]%100)))
	# fout.close()
# getStream()
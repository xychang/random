function GetParam(paramName) {
	paramValue = "";
	isFound = false;
	if (this.location.search.indexOf("?") == 0 && this.location.search.indexOf("=") > 1) {
		arrSource = unescape(this.location.search).substring(1, this.location.search.length).split("&");
		i = 0;
		while (i < arrSource.length && !isFound) {
			if (arrSource[i].indexOf("=") > 0) {
				if (arrSource[i].split("=")[0].toLowerCase() == paramName.toLowerCase()) {
					paramValue = arrSource[i].split("=")[1];
					isFound = true;
				}
			}
			i++;
		}
	}
	return paramValue;
}

function SetCookie(name, value) {
	var Days = 30;
	var exp = new Date();
	exp.setTime(exp.getTime() + Days * 24 * 60 * 60 * 1000);
	document.cookie = name + "=" + escape(value) + ";expires=" + exp.toGMTString();
}

function getCookie(name) {
	var arr = document.cookie.match(new RegExp("(^| )" + name + "=([^;]*)(;|$)"));
	if (arr != null) return unescape(arr[2]);
	return null;
}

function OnlyLZ() {
	form = document.body.getElementsByTagName("form");
	for (i = 0; i < form.length; i++) {
		if (form[i].name == "modform") {
			form = form[i];
			break;
		}
	}
	ID = GetParam('t');
	divList = form.getElementsByTagName("div");
	LZ = getCookie(ID);
	if (!LZ) {
		LZ = divList[0].getElementsByClassName("{$post[");
		LZ = LZ[0].textContent;
		SetCookie(ID, LZ);
	}
	for (i = 0; i < divList.length; i++) {
		nameSpan = divList[i].getElementsByClassName("{$post[");
		if (nameSpan.length == 0) {
			continue;
		}
		if (nameSpan[0].textContent != LZ) {
			divList[i].hidden = true;
		}
	}
}
OnlyLZ();
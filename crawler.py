#!/usr/bin/env python
# -*- coding: utf-8 -*-
# crawl weibo
import requests, re, json, datetime, smtplib, time
from bs4 import BeautifulSoup
from email.mime.text import MIMEText

weiboHeader = {
	'Accept' : 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
	'Acept-Encoding' : 'gzip, deflate, sdch',
	'Accept-Lenguage' : 'zh-CN,zh;q=0.8,en;q=0.6,zh-TW;q=0.4',
	'Chrome-Proxy' : 'ps=1431142059-3634568314-3681684348-3978403149, sid=955e78ae4745cbe1c52418f08edc7ea6, c=mac, b=2311, p=135',
	'User-Agent' : 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36'
}

def sendEmail(fin):
	data = fin.read()
	data = re.sub('\n', '\n<br>\n', data)
	msg = MIMEText(data, 'html', 'utf-8')
	fin.close()
	msg['Subject'] = '新微博 %s' % datetime.date.today()
	msg['from'] = 'xiaoyun.anye@gmail.com'
	msg['To'] = 'xiaoyun.anye@gmail.com, frostmirror@gmail.com'

	s = smtplib.SMTP('smtp.gmail.com:587')
	s.ehlo()
	s.starttls()
	s.login('xiaoyun.anye@gmail.com', open('apppass.txt').read().strip())
	s.sendmail('xiaoyun.anye@gmail.com', ['xiaoyun.anye@gmail.com','frostmirror@gmail.com'], msg.as_string())
	# s.sendmail('xiaoyun.anye@gmail.com', ['xiaoyun.anye@gmail.com'], msg.as_string())
	s.quit()

def hasKeyword(text, word):
	return text.find(word) >= 0

def findNewWeibos():
	try:
		existSet = json.load(open('weibos.json'))
	except:
		existSet = []
	# print(existSet)

	fout = open('tmp.txt', 'w')
	s = requests.Session()
	resp = s.get('http://s.weibo.com/weibo/%25E6%2589%25AB%25E6%2596%2587%25E5%25B0%258F%25E9%2599%25A2', \
		timeout = 300, headers=weiboHeader)
	text = resp.text
	matches = re.findall(r'"html":"(<div class=\\"search_feed.+)"', text)
	count = 0
	for div in matches:
		div = (div.decode('unicode-escape'))
		div = div.encode('utf8')
		div = re.sub(r'\\/','/', div)
		soup = BeautifulSoup(div)
		for item in (soup.select('div.feed_content')):
			# get post url
			fromUrl = item.find_next_sibling().a.get('href').encode('utf8')

			if not hasKeyword(item.get_text().encode('utf8'), '扫文小院'):
				continue

			if (fromUrl in existSet):
				# print('exist')
				continue
			else:
				print(fromUrl)
				existSet.append(fromUrl)

			# remove stats of the original post
			map(lambda x: x.decompose(), item.select('.feed_func'))

			# replace all the icons
			icons = item.select('.comment_txt img')
			for icon in icons:
				alt = icon.get('alt')
				if not alt:
					continue
				icon.replace_with(icon.get('alt'))

			# find the attached images
			# print(item)
			images = [image.get('src') for image in item.select('.media_box li img') if not image.get('class') or 'loading_gif' not in image.get('class')]
			# transform the image in the link
			imagesText = ''.join(['<img src="%s">' % re.sub('thumbnail', 'large', x) for x in images])
			# print(images)
			# print(item)
			text = '%s\n%s\n%s:<a href="%s">%s</a>'%(item.get_text().encode('utf8'),imagesText.encode('utf8'), '微博地址',fromUrl, fromUrl)
			# text = '%s\n%s\n%s:<a href="%s">%s</a>'%(item.get_text().encode('utf8'),imagesText, '微博地址',fromUrl, fromUrl)
			# remove unnecessary blank spaces
			text = re.sub(r'(\n\s*)+', '\n', text)
			text = re.sub(r'[\t ]+', ' ', text)

			fout.write(text)
			fout.write('\n-------\n')
			count += 1
			# break
	fout.close()
	json.dump(existSet, open('weibos.json', 'w'))
	if count > 0:
		sendEmail(open('tmp.txt'))

while(True):
	findNewWeibos()
	time.sleep(7200)
// Load the script
var script = document.createElement("SCRIPT");
script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js';
script.type = 'text/javascript';
document.getElementsByTagName("head")[0].appendChild(script);

// Poll for jQuery to come into existance
var checkReady = function(callback) {
    if (window.jQuery) {
        callback(jQuery);
    }
    else {
        window.setTimeout(function() { checkReady(callback); }, 100);
    }
};

function getCheckValid(authorN, contentMin){
	return function(author, content){
		return (author == authorN) || (content.length > contentMin);
	}
}

// Start polling...
checkReady(function(){
	var author = $('span.normalname, span.unreg');
	if (author.length <= 0){
		author = $('span[class^="{$post"');
	}
	author = author.eq(0).text();
	console.log(author);
	var result = grabTxt(document, getCheckValid(author, 1000));
	// go through all pages
	if ($('.pagelink').length <= 0){
		console.log(result);
		return;
	}
	var pageData = [];
	var pageInfo;
	$('.pagelink').each(
		function(){
			pageInfo = $(this).find('a').attr('href').match(/(.*pp=)(\d+)/);
			if (pageInfo){
				console.log($(this).find('a').attr('href'), pageInfo);
				pageData.push(parseInt(pageInfo[2]));
			}		
		});
	var lastPage = Math.max.apply(Math, pageData);
	var start = 50;
	var results = new Array(lastPage / start + 1);
	results[0] = result;
	$(document).data('results', results);
	while(start <= lastPage){
		var idx = start / 50;
		$.ajax(pageInfo[1] + start,
		{
			success: funcPutAt(idx, author, results)
		});
		start += 50;
	}
});

function funcPutAt(idx, author, results){
	return function(html){
		console.log('running ' + idx);
		text = grabTxt(html, getCheckValid(author, 1000));
		results[idx] = text;
		console.log(results);
		if (results.filter(String).length == results.length){
			// console.log(results.join('\n\n'));
			$('#postW').val(results.join('\n\n'));
		}
	};
}

function grabTxt(html, checkValid){
	var result = ' ';
	$(html).find('div[id^=table_]').each(function(){
		$(this).find('iframe').remove();
		var content = $(this).find('.postcontent').append('<br />');
		content.find('br').replaceWith('\n');
		content = content.text();
		content = content.replace(/\n{2,}/g, '\n');
		var author = $(this).find('span.normalname, span.unreg');
		if (author.length <= 0){
			author = $(this).find('span[class^="{$post"');
		}
		author = author.text();
		if (checkValid(author, content)){
			result += content + '\n\n';
		}
	});
	return result;
}
// Load the script
var script = document.createElement("SCRIPT");
script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js';
script.type = 'text/javascript';
document.getElementsByTagName("head")[0].appendChild(script);

// Poll for jQuery to come into existance
var checkReady = function(callback) {
    if (window.jQuery) {
        callback(jQuery);
    }
    else {
        window.setTimeout(function() { checkReady(callback); }, 100);
    }
};

function getCheckValid(authorN, contentMin){
	return function(author, content){
		return (author == authorN) || (content.length > contentMin);
	}
}

// Start polling...
checkReady(function(){
	var author = jQ('.pls.favatar .pi');
	author = jQ.trim(author.eq(0).text());
	console.log(author);
	var result = grabTxt(document, getCheckValid(author, 1000));
	// go through all pages
	if (jQ('.pg').length <= 0){
		console.log(result);
		return;
	}
	var pageData = [];
	var pageInfo;
	jQ('.pg a').each(
		function(){
			pageInfo = jQ(this).attr('href').match(/(.*page=)(\d+)/);
			if (pageInfo){
				console.log(jQ(this).attr('href'), pageInfo);
				pageData.push(parseInt(pageInfo[2]));
			}		
		});
	var lastPage = Math.max.apply(Math, pageData);
	var start = 2;
	var results = new Array(lastPage);
	results[0] = result;
	jQ(document).data('results', results);
	while(start <= lastPage){
		var idx = start - 1;
		jQ.ajax(pageInfo[1] + start,
		{
			success: funcPutAt(idx, author, results)
		});
		start += 1;
	}
});

function funcPutAt(idx, author, results){
	return function(html){
		console.log('running ' + idx);
		text = grabTxt(html, getCheckValid(author, 1000));
		results[idx] = text;
		console.log(results);
		if (results.filter(String).length == results.length){
			// console.log(results.join('\n\n'));
			jQ('#fastpostmessage').val(results.join('\n\n'));
		}
	};
}

function grabTxt(html, checkValid){
	var result = ' ';
	// jQ('div[id^=post_]:not([id^=post_rate]) .pls.favatar .pi').each(function(){ var author = jQ.trim(jQ(this).text()); console.log(author); })
	jQ(html).find('div[id^=post_]:not([id^=post_rate])').each(function(){
		jQ(this).find('iframe').remove();
		var content = jQ(this).find('td[id^=postmessage_]').append('<br />');
		content.find('br').replaceWith('\n');
		content = content.text();
		content = content.replace(/\n{2,}/g, '\n');
		var author = jQ(this).find('.pls.favatar .pi');
		author = jQ.trim(author.text());
		if (checkValid(author, content)){
			result += content + '\n\n';
		}
	});
	return result;
}
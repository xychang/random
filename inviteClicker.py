import requests, json, time, re
import bs4
from bs4 import BeautifulSoup

try:
	proxies = json.load(open('proxies.json'))
except:
	proxies = []

try:
	used = json.load(open('used.json'))
except:
	used = []

def getFromHide(s):
	hideUrl = 'http://proxylist.hidemyass.com/search-1302870#listable'

	resp = s.get(hideUrl)
	resp = resp.text
	# print(resp.encode('utf-8'))
	# resp = open('test.html').read()
	soup = BeautifulSoup(resp)
	getStyle = r'\.([\w-]+)\{display:(none|inline)\}'

	proxies = []

	for row in soup.select('#listable tbody tr'):
		host = row.td.next_sibling.next_sibling
		styles = host.span.style.text.strip().split()
		classMap = {}
		for style in styles:
			styleR = re.match(getStyle, style)
			classMap[styleR.group(1)] = styleR.group(2)
		# print(host)
		hostS = ''
		for item in host.span.children:
			if type(item) is bs4.element.NavigableString:
				hostS += item
				continue
			if (item.name == 'style'):
				continue
			if item.has_attr('style') and item['style'] == 'display:none':
				continue
			itemClass = item['class'][0] if item.has_attr('class') else None
			if itemClass:
				if itemClass in classMap and classMap[itemClass] == 'none':
					continue
			hostS += item.text
		# print(hostS)

		port = host.next_sibling.next_sibling.text.strip()
		protocol = host.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.text.strip()
	
		proxies.append({protocol.lower(): '%s://%s:%s' % (protocol, hostS, port)})
	print(proxies)
	return proxies

def getProxies():
	yasUrl = 'http://www.yasakvar.com/apiv1/?type=json'
	theUrl = 'https://theproxisright.com/api/proxy/get?onlyActive=true&maxResults=10000&apiKey=67ab3c0b-d629-4391-aebf-679c2dcb5e5f'
	hideUrl = 'http://proxylist.hidemyass.com/search-1302870#listable'

	s = requests.Session()
	try:
		resp = s.get(yasUrl)
		resp = resp.text
		resp = json.loads(resp)
		if (resp['success'] == 'true'):
			for proxy in resp['proxylist']:
				proxy = resp['proxylist'][proxy]
				host = ('%s://%s:%s' % (proxy['protocol'], proxy['ip'], proxy['port'])).lower()
				if not host in used:
					proxies.append({proxy['protocol'].lower(): host})
	except:
		print('load yas failed')
	
	resp = s.get(theUrl, verify=False)
	resp = resp.text
	try:
		resp = json.loads(resp)
		if (resp['list']):
			for proxy in resp['list']:
				if (proxy['type'].startswith('http')):
					host =  ('%s://%s' % (proxy['type'], proxy['host'])).lower()
					if not host in used:
						proxies.append({proxy['type'].lower():host})
	except:
		print('load theproxisright failed')

	proxies.extend(getFromHide(s))
	
	json.dump(proxies, open('proxies.json', 'w'))


def makeRequest():
	proxy = proxies.pop()
	if proxy.items()[0][1] in used:
		return
	url = 'http://www.jjwxc.net/backend/invite.php?inviteId=2011197'
	url = 'http://www.jjwxc.net/backend/invite.php?inviteId=2026085'
	s = requests.Session()
	try:	
		resp = s.get(url, timeout = 20, proxies = proxy)
	except:
		return
	resp.encoding = 'gb18030'
	soup = BeautifulSoup(resp.text)
	print(soup.text[:500].encode('utf-8'))
	# proxies.insert(0, proxy)
	# except:
	# 	print('failed with ', proxy)
	used.append(proxy.items()[0][1])

while True:
	for idx in range(100):
		if (len(proxies) < 10):
			getProxies()
		if (len(proxies) < 10):
			print('not enough proxies')
			break
		makeRequest()
		time.sleep(5)
	json.dump(proxies, open('proxies.json', 'w'))
	json.dump(used, open('used.json', 'w'))
	time.sleep(3600)
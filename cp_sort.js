

// Load the script
var script = document.createElement("SCRIPT");
script.src = 'http://apps.bdimg.com/libs/jquery/1.9.1/jquery.min.js';
script.type = 'text/javascript';
document.getElementsByTagName("head")[0].appendChild(script);

// Poll for jQuery to come into existance
var checkReady = function(callback) {
    if (window.jQuery) {
        callback(jQuery);
    }
    else {
        window.setTimeout(function() { checkReady(callback); }, 100);
    }
};

function getCheckValid(authorN, contentMin){
	return function(author, content){
		return (author == authorN) || (content.length > contentMin);
	}
}

// Start polling...
checkReady(function(){
	$('.ttable td:nth-child(5)').each(function(){
	    var date = $(this).text().match(/\d{4}\-\d{2}\-\d{2}\s+\d{2}:\d{2}/);
	    if (date){
		    $(this).closest('tr').attr('date-update', 'd-'+date);
	    }
	});
	var trs = $('.ttable tr[date-update^="d-"]');
	var start = trs.eq(0).prev();
	trs.sort(function(a, b){
		var ad = a.getAttribute('date-update'),
			bd = b.getAttribute('date-update');
		if (ad > bd){
			return -1;
		}
		if (bd > ad){
			return 1;
		}
		return 0;
	});
	trs.detach().insertAfter(start);
});